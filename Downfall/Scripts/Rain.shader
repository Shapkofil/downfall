shader_type canvas_item;

uniform sampler2D rain_texture;
uniform float intensity : hint_range(0.0,1.0) = 0.05;
uniform float angle : hint_range(-1.57, 1.57) = 0.0;
uniform float speed: hint_range(0.1, 3.0) = 1.0;


void fragment() {
	float alpha = angle - 3.14/2.0;
	vec2 direction = vec2(cos(alpha), sin(alpha));
	float movement = TIME * speed;
	vec4 displacement = texture(rain_texture, fract(SCREEN_UV - direction * movement));
	float displacement_length = length(displacement.rgb);
	vec2 uv = SCREEN_UV + displacement.rg * displacement_length * intensity * texture(TEXTURE, UV).r; //Add the texture here
	COLOR = vec4(texture(SCREEN_TEXTURE, uv).rgb, 1.0);
}