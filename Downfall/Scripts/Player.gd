extends Node2D

var rain
var scoreboard
var sprite
var popup
var ground
var foreground

var umbra = false
var death = false

export var PUSSY_LIMIT = 4.0
export var INIT_SPEED = .2
export var SPEED_MULT = .5
export var MIN_DEATH_TIME = .2

var base_speed = INIT_SPEED
var speed = 0
var score = 0
var checkpoint = 0
var hiscore = 0
var death_clock = 0
var pussy_clock = 0


# Called when the node enters the scene tree for the first time.
func _ready():
	rain = get_parent().get_node("Rain")
	scoreboard = get_parent().get_node("ScoreBoard")
	popup = get_parent().get_node("Popup")
	sprite = get_node("AnimatedSprite")

	foreground = get_parent().get_node("ParallaxBackground").get_node("Foreground").get_node("Visual").get_node("AnimationPlayer")
	ground = get_parent().get_node("ParallaxBackground").get_node("Ground").get_node("Visual").get_node("AnimationPlayer")

	death_state()

func _process(delta):

	if Input.is_action_just_pressed("Duck"):
		umbra = not umbra
		undeath_state()

	# Deal with base speed
	death_clock += delta
	base_speed += delta * 0.4

	# Deal animations
	foreground.playback_speed = speed * 0.002 + 0.5
	ground.playback_speed = speed * 0.002 + 0.5

	if not death:
		if umbra:
			score += delta * 3
			speed = base_speed
			sprite.play("umbra-loop")
		else:
			score += delta * 5
			speed = base_speed * SPEED_MULT
			sprite.play("noumbra-loop")
			pussy_clock = 0

	if rain.sample() < 0 and not death and not umbra:
		popup.get_node("Label").text = "Press\n[S] or [down]\nto play"
		death_state()

	if rain.sample() > 0 and umbra:
		pussy_clock += delta

	if pussy_clock > PUSSY_LIMIT and not death:
		pussy_state()

	update_score()


func pussy_state():
	popup.get_node("Label").text = "C'MON PUSSY\nUSE THE\nUMBRELLA\nLESS"
	death_state()

func death_state():
	if not death:
		death_clock = 0

	popup.popup()
	get_tree().paused = true

	base_speed = INIT_SPEED
	speed = 0 
	death = true

	if score > hiscore:
		hiscore = score
	score = 0
	checkpoint = 0

func undeath_state():
	if death_clock < MIN_DEATH_TIME:
		umbra = not umbra
		return

	popup.hide()
	get_tree().paused = false
	death = false

func update_score():
	scoreboard.text = "HI " + get_score(hiscore) + " " + get_score(score)

	if checkpoint < int(score/100):
		scoreboard.get_node("Horn").play()
		checkpoint+=1

func get_score(x):
	return "%05d" % int(x)
