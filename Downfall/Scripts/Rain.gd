extends TextureRect


var noise = OpenSimplexNoise.new()
var current = 0
var tex_offset
var player

export var NOISE_INFLUENCE = .2
export var COS_SCALE = 256.0
export var RESPONCE_TIME = .2
export var RAIN_SPEED = 256.0
var rainspeed = 0


func controlled_noise(i):
	return noise.get_noise_2d(i,i) * NOISE_INFLUENCE + cos(i / COS_SCALE) * (1 - NOISE_INFLUENCE)

func _ready():
	noise.seed = randi()
	noise.octaves = 4
	noise.period = 20.0
	noise.persistence = 0.8

	player = get_parent().get_node("Player")
	tex_offset = player.position[0]

func sample():
	return max(controlled_noise(current - RESPONCE_TIME),
			   controlled_noise(current + RESPONCE_TIME))

func gen_mask():
	var image = Image.new()
	image.create(256, 1, false, 2)
	var offset = -tex_offset + current
	image.lock()
	for i in range(256):
		var value = 0
		if controlled_noise(i + offset) < 0:
			value = 1
		image.set_pixel(i,0,Color(1 * value,1,1,1))
	image.unlock()

	var image_texture = ImageTexture.new()
	image_texture.create_from_image(image)
	return image_texture


func _process(delta):
	rainspeed = player.speed * RAIN_SPEED
	current += delta * rainspeed

	texture = gen_mask()
